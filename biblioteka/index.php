<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Library</title>
    <link href="bootstrap.min.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="index.php">Library</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="index.php">главная <span class="sr-only"></span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="add.php">добавить книгу</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="edit.php">показать книгу</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="delete.php">удалить книгу</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <?php
    $countOfSymbols = strripos($_SERVER['REQUEST_URI'],'/');
    $file = substr($_SERVER['REQUEST_URI'],$countOfSymbols+1);
    if ($file=="index.php") {
        $db = mysqli_connect('127.0.0.1', 'root', '', 'books_app');
        $books =$db->query("SELECT * FROM `books`");

        while ($row = $books->fetch_assoc())
        {
            $str.='<tr>
                        <td>'.$row["id"].'</td>
                        <td>'.$row["name"].'</td>
                        <td>'.$row["autor"].'</td>
                        <td>'.$row["year"].'</td>
                   </tr>';
        }

        echo '<div>

                    <h1>All books in the library</h1>
                    
                     <table align="center" width="50%"  cellpadding="5">
                         <tr>
                             <th>id книги</th>
                             <th>название книги</th>
                             <th>автор</th>
                             <th>дата выпуска</th>
                         </tr>
                         '.$str.'
                     </table>
                     
                 </div>';
    }
    ?>
</div>
</body>
</html>