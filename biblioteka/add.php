<?php
include "index.php";
echo '<div><br><br><br>
        <p>для добавления книги введите ее название, автора и дату выхода, после загрузите файл (.txt/.pdf)</p>
        <form action="store.php" method="post" enctype="multipart/form-data">
            <label for="name">название книги</label>
            <input required type="text" name="name"><br><br>
            <label for="date">дата выхода</label>
            <input required type="date" name="date"><br><br>
            <label for="autor">автор книги</label>
            <input required type="text" name="autor"><br><br>
            <label for="file">загрузите файл</label>
            <input required type="file" name="file"><br><br>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Добавить книгу</button>
        </form>
    </div>';
